﻿using System.Collections.Generic;
using System;
using Assets.Scripts;
using System.Linq;

namespace Assets.Scripts.Legacy
{
    [Serializable]
    public class Clothes
    {
        public List<Clothing> clothes;
        public List<string> categories;

        public Clothes()
        {
            clothes = new List<Clothing>();
            categories = new List<string>();
        }

        public void Merge(Clothes item)
        {
            clothes.AddRange(item.clothes);
            categories = categories.Concat(item.categories).Distinct().ToList();
        }
    }

    [Serializable]
    public class CommonParameters
    {
        public bool RemoveVideoBackground = true;
        public bool Smoothing = true;
        public int SmoothingP1 = 2;
        public int SmoothingP2 = 2;
        public bool UseOnlyKinect = false;
        public string DescriptionTitle = "Store";
        public string DescriptionStore = "Description";
        public string CameraName = "Logitech HD Pro WebCam C920";
        public string[] Hints = new string[2]
        {
      "Hier klicken um Kleidungsstück zu wählen" ,
      "Hier klicken um Kategorie zu wählen"
        };
        public float pauseCarousel = 1.5f;
        public float clickTime = 0.5f;
        public float pauseCarouselBetweenClicks = 1.5f;
        public float fixingTimeClothPosition = 5f;
        public float showTimeClothPosition = 1f;
        public float timerStepClothPosition = 0.5f;
        public float autoPositionOffset = -50f;
    } 
}