﻿using UnityEngine;
using System.Collections;
using System.IO;
using System.Text.RegularExpressions;
using Paths = PathLoader;

public class ParameterManagerBase<P> : MonoBehaviour
{
    public string filename;
    [HideInInspector]
    public P parameters;
    public delegate void AfterLoad();
    public AfterLoad OnLoad;
    private bool isLoaded = false;
    public bool IsLoaded { get { return isLoaded; } protected set { isLoaded = value; } }
    public P Parameters { get { LoadIfNotLoaded(); return parameters; } }

    public virtual bool Load()
    {
        var filepath = Path.Combine(Paths.commonDataPath, filename);
        if (IsValidFilename(filepath))
        {
            parameters = UnityXMLSerializer.DeserializeFromXMLFile<P>(filepath);
            Debug.Log("File " + filepath + " is " + (parameters == null ? "not loaded!" : "loaded!"));
            IsLoaded = parameters != null;
            return parameters != null;
        }
        else
            return false;
    }

    public virtual void LoadIfNotLoaded()
    {
        if (!IsLoaded)
        {
            Load();
            if (isLoaded && OnLoad != null)
            {
                OnLoad();
            }
        }
    }

    public bool Save()
    {
        var filepath = Path.Combine(Paths.commonDataPath, filename);
        if (IsValidFilename(filepath))
            return UnityXMLSerializer.SerializeToXMLFile<P>(filepath, parameters);
        else
            return false;
    }

    bool IsValidFilename(string testName)
    {
        Regex containsABadCharacter = new Regex("["
              + Regex.Escape(new string(System.IO.Path.GetInvalidPathChars())) + "]");
        if (containsABadCharacter.IsMatch(testName)) { return false; };
        return true;
    }

    public static string CreateId()
    {
        return "{" + System.Guid.NewGuid().ToString().ToLower() + "}";
    }

}
