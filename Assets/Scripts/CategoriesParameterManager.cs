﻿using UnityEngine;
using System.Collections.Generic;
using System.Linq;

public class CategoriesParameterManager : ParameterManagerBase<CategoriesData> {
    public static CategoriesParameterManager instance;
    void Awake()
    {
        instance = this;
    }
	// Use this for initialization
	void Start () {
        LoadIfNotLoaded();
    }

    public Dictionary<string, string> GetCategoriesForCurrentLanguage()
    {
        var currLanguage = LanguageParameterManager.instance.GetCurrentLanguageId();
        var cats = parameters.data.First(p => p.languageId == currLanguage);
        var dict = cats.titles.ToDictionary(p => p.categoryId, p => p.title);
        return dict;
    }
}
