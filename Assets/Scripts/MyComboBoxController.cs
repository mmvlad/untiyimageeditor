﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.UI;
using System.Linq;

public class MyComboBoxController : MonoBehaviour
{
    [SerializeField]
    private GameObject      _panel;
    [SerializeField]
    private Button          _btnPref;

    [SerializeField]
    private Text            _choiceText;

    private string _selectedId;
    //private List<string>    _options = new List<string>();
    private Dictionary<string, string> _dictOptions = new Dictionary<string, string>();

    public ContentSizeFitter fitter;
    public delegate void SelectFinish(string text);
    public SelectFinish OnSelectFinish;

    public void setText(string text)
    {
        _choiceText.text = text;
        _panel.SetActive(false);
        var key = _dictOptions.First(p => p.Value == text).Key;
        _selectedId = key;
        if (OnSelectFinish != null)
        {
            Debug.Log("setText = " + text);
            OnSelectFinish(key);
        }
    }

    public void addOption(string key, string value)
    {
        _dictOptions[key] = value;

        _panel.SetActive(true);

        Button btn = Instantiate(_btnPref);

        Text txt = btn.GetComponentInChildren<Text>();
        txt.text = value;

        btn.GetComponent<Button>().onClick.AddListener(() => {
            setText(btn.GetComponentInChildren<Text>().text);
        });


        if (fitter != null)
        {
            btn.transform.SetParent(fitter.transform);
        }

        updateFirst();

        _panel.SetActive(false);

    }

    private void updateFirst()
    {
        var first = _dictOptions.First();
        _choiceText.text = first.Value;
        _selectedId = first.Key;
    }



    public void clearAll()
    {
        _dictOptions.Clear();
        _panel.SetActive(true);

        if (fitter != null)
        {
            foreach (Transform t in fitter.transform)
            {
                Destroy(t.gameObject);
            } 
        }

        _panel.SetActive(false);

        _choiceText.text = "";
    }

    public string getSelectedOptionName()
    {
        return _choiceText.text;
    }

    public string getSelectedId()
    {
        return _selectedId;
    }

    public void removeOptionByName(string name)
    {
        var item = _dictOptions.First(p => p.Value == name);

        _dictOptions.Remove(item.Key);

        _panel.SetActive(true);

        ContentSizeFitter fitter = _panel.GetComponentInChildren<ContentSizeFitter>();

        foreach (Transform t in fitter.transform)
        {
            Text btnText = t.GetComponentInChildren<Text>();
            if (btnText.text == name)
            {
                Destroy(t.gameObject);
                break;
            }
        }
        if (_dictOptions.Count > 0)
        {
            updateFirst();
        }
        else
        {
            _choiceText.text = "";
        }
        

        _panel.SetActive(false);
    }

    public void removeOptionByNameFromDictionary(string name)
    {

        //int index = _options.FindIndex(x => x == name);
        //if (index == -1)
        //{
        //    return;
        //}

        //_options.Remove(name);

        //_panel.SetActive(true);

        //ContentSizeFitter fitter = _panel.GetComponentInChildren<ContentSizeFitter>();

        //foreach (Transform t in fitter.transform)
        //{
        //    Text btnText = t.GetComponentInChildren<Text>();
        //    if (btnText.text == name)
        //    {
        //        Destroy(t.gameObject);
        //        break;
        //    }
        //}
        //if (_options.Count > 0)
        //{
        //    updateFirst();
        //}
        //else
        //{
        //    _choiceText.text = "";
        //}


        //_panel.SetActive(false);
    }

    //public void removeOptionByIndex(int index)
    //{
    //    if (index < 0 || index >= _options.Count)
    //    {
    //        Debug.LogError("Index out of range");
    //        return;
    //    }

    //    string name = _options[index];
    //    removeOptionByName(name);
    //}

    public void removeSelected()
    {
        removeOptionByName(getSelectedOptionName());
    }

    //public List<string> ListOfOptions
    //{
    //    get
    //    {
    //        return new List<string>(_options);
    //    }

    //    set
    //    {
    //        List<string> newList = value;
    //        clearAll();

    //        foreach(string option in newList)
    //        {
    //            addOption(option);
    //        }
    //    }
    //}

    public Dictionary<string, string> DictionaryOfOptions
    {
        get
        {
            return _dictOptions;
        }
        set
        {
            clearAll();
            foreach (var item in value)
            {
                addOption(item.Key, item.Value);
            }
        }
    }

}
