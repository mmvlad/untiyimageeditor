﻿using UnityEngine;
using System;
using System.Collections.Generic;
using System.Linq;
using Assets.Scripts;

[Serializable]
public class CommonStringData
{
    public string languageId;
    public string DescriptionTitle = "Store";
    public string DescriptionStore = "Description";
    public List<string> Hints = new List<string>();
    public CommonStringData Clone()
    {
        return this.MemberwiseClone() as CommonStringData;
    }
}

[Serializable]
public class CommonDigitalData
{
    public bool RemoveVideoBackground = true;
    public bool Smoothing = true;
    public int SmoothingP1 = 2;
    public int SmoothingP2 = 2;
    public bool UseOnlyKinect = false;
    public string CameraName = "Logitech HD Pro WebCam C920";
    public float pauseCarousel = 1.5f;
    public float clickTime = 0.5f;
    public float pauseCarouselBetweenClicks = 1.5f;
    public float fixingTimeClothPosition = 5f;
    public float showTimeClothPosition = 1f;
    public float timerStepClothPosition = 0.5f;
    public float autoPositionOffset = -50f;
    public float clothingPositionXScale = 1.92f;
    public float clothingPositionYScale = 1f;
    public Vector3 rightShoulderJoint = new Vector3();
    public Vector3 leftShoulderJoint = new Vector3();
    public Vector3 neckJoint = new Vector3();
    public Vector3 baseSpineJoint = new Vector3();
}

[Serializable]
public class CommonData
{
    public string activeLanguageId;
    public CommonDigitalData digitalData = new CommonDigitalData();
    public List<CommonStringData> stringData = new List<CommonStringData>();
    public CommonStringData GetDataByLanguageId(string langId)
    {
        return stringData.First(p => p.languageId == langId);
    }
    public CommonStringData CurrentStringData
    {
        get
        {
            return GetDataByLanguageId(LanguageParameterManager.instance.GetCurrentLanguageId());
        }
    }
    public CommonData Clone()
    {
        return this.MemberwiseClone() as CommonData;
    }
}
