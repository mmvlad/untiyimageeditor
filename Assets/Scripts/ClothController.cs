﻿using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(RectTransform))]
public class ClothController : MonoBehaviour
{
    /// <summary>
    /// The _scale slider
    /// </summary>
    [SerializeField]
    private Slider _scaleSlider;
    /// <summary>
    /// The _rect
    /// </summary>
    private RectTransform _rect;
    private Vector3 _dragOffset;
    private float _lastVal = 0;
    /// <summary>
    /// The scale step
    /// </summary>
    public float ScaleStep = 0.01f;
    /// <summary>
    /// The motion step
    /// </summary>
    public float MotionStep = 1f;

    /// <summary>
    /// Awakes this instance.
    /// </summary>
    private void Awake()
    {
        _rect = GetComponent<RectTransform>();

        if (_scaleSlider == null)
        {
            Debug.LogError("Scale slider is null");
        }
    }
    /// <summary>
    /// Begins the drag.
    /// </summary>
    public void BeginDrag()
    {
        _dragOffset = _rect.position - Input.mousePosition;
    }
    /// <summary>
    /// Called when /[drag].
    /// </summary>
    public void OnDrag()
    {
        _rect.position = Input.mousePosition + _dragOffset;
    }
    /// <summary>
    /// Scales the value changed.
    /// </summary>
    public void scaleValueChanged()
    {
        float newVal = _scaleSlider.value;
        float delta = newVal - _lastVal;
        _lastVal = newVal;
        var oldScale = new Vector3(_rect.localScale.x, _rect.localScale.y, _rect.localScale.z);
        Vector3 scale = _rect.localScale;
        scale += new Vector3(delta, delta, 1);
        _rect.localScale = scale;
        if (MainController.instance.isTopbased)
        {
            var diff = _rect.sizeDelta.y * (scale.y - oldScale.y);
            _rect.position -= new Vector3(0, diff, 0);
        }
        Debug.Log("Scale =" + _scaleSlider.value);

    }

    // BUTTON ACTION    
    /// <summary>
    /// Moves the left.
    /// </summary>
    public void MoveLeft()
    {
        _rect.localPosition -= new Vector3(MotionStep, 0);
    }

    // BUTTON ACTION    
    /// <summary>
    /// Moves the right.
    /// </summary>
    public void MoveRight()
    {
        _rect.localPosition += new Vector3(MotionStep, 0);
    }

    // BUTTON ACTION    
    /// <summary>
    /// Moves up.
    /// </summary>
    public void MoveUp()
    {
        _rect.localPosition += new Vector3(0, MotionStep);
    }

    // BUTTON ACTION    
    /// <summary>
    /// Moves down.
    /// </summary>
    public void MoveDown()
    {
        _rect.localPosition -= new Vector3(0, MotionStep);
    }

    // BUTTON ACTION    
    /// <summary>
    /// Incs the scale value.
    /// </summary>
    public void IncScaleValue()
    {
        _scaleSlider.value += ScaleStep;
        Debug.Log("Scale =" + _scaleSlider.value);
    }

    // BUTTON ACTION    
    /// <summary>
    /// Decimals the scale value.
    /// </summary>
    public void DecScaleValue()
    {
        _scaleSlider.value -= ScaleStep;
        Debug.Log("Scale =" + _scaleSlider.value);
    }
    /// <summary>
    /// Resets this instance.
    /// </summary>
    public void reset()
    {
        _lastVal = 0;
        _scaleSlider.value = 0;
        _rect.localScale = Vector3.one;
        _rect.transform.localPosition = Vector3.zero;
    }

    /// <summary>
    /// Applies the props.
    /// </summary>
    /// <param name="offsetX">The offset x.</param>
    /// <param name="offsetY">The offset y.</param>
    /// <param name="scale">The scale.</param>
    public void applyProps(float offsetX, float offsetY, float scale)
    {
        Vector3 localPos = new Vector3(offsetX, offsetY, 0);
        _rect.transform.localPosition = localPos;
        _rect.localScale = new Vector3(scale, scale, 1);



        if (scale >= 1)
        {
            float sliderVal = scale - 1;
            _lastVal = sliderVal;
            _scaleSlider.value = sliderVal;
        }
        else
        {
            float sliderVal = -(1 - scale);
            _lastVal = sliderVal;
            _scaleSlider.value = sliderVal;
        }
        Debug.Log("Scale =" + _scaleSlider.value);

    }
}
