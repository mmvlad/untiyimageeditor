﻿using UnityEngine;
using System.IO;
using System;
using ICSharpCode.SharpZipLib.Zip;
using System.Diagnostics;
using System.Collections.Generic;
namespace Assets.Scripts
{
    public class Utils
    {
        public static void copyTo(string from, string to)
        {
            File.Copy(from, to, true);
        }

        public static void delete(string filename)
        {
            File.Delete(filename);
        }

        public static bool isXml(string path)
        {
            return Path.GetExtension(path) == Constants.EXT_XML;
        }


        public static bool IsCorrectFormat(string path)
        {
            string e = Path.GetExtension(path);
            if (e != Constants.EXT_PNG)
            {
                return false;
            }
            return true;
        }

        public static Texture2D LoadPNG(string filePath)
        {
            Texture2D tex = null;

            if (File.Exists(filePath))
            {
                var fileData = File.ReadAllBytes(filePath);
                tex = new Texture2D(2, 2);
                tex.LoadImage(fileData); //..this will auto-resize the texture dimensions.
            }
            return tex;
        }

        private static Dictionary<string, Sprite> cache = null;

        public static Sprite SpriteFromFile(string filePath)
        {
            if (cache == null)
            {
                cache = new Dictionary<string, Sprite>();
            }
            if (cache.ContainsKey(filePath))
            {
                return cache[filePath];
            }
            Sprite s = Utils.SpriteFromTex2d(Utils.LoadPNG(filePath));
            cache[filePath] = s;
            return s;
        }

        public static Sprite SpriteFromTex2d(Texture2D tex)
        {
            Texture2D old = tex;
            Texture2D left = new Texture2D((int)(old.width), old.height, old.format, false);
            Color[] colors = old.GetPixels(0, 0, (int)(old.width), old.height);
            left.SetPixels(colors);
            left.Apply();
            Sprite sprite = Sprite.Create(left,
                   new Rect(0, 0, left.width, left.height),
                   new Vector2(0.5f, 0.5f),
                   40);
            return sprite;
        }

        public static void CompressDirToZip(string dirToCompress, string zipPath)
        {
            string[] filenames = Directory.GetFiles(dirToCompress);

            using (ZipOutputStream s = new ZipOutputStream(File.Create(zipPath)))
            {
                s.SetLevel(9); // 0 - store only to 9 - means best compression

                byte[] buffer = new byte[4096];

                foreach (string file in filenames)
                {

                    // Using GetFileName makes the result compatible with XP
                    // as the resulting path is not absolute.
                    ZipEntry entry = new ZipEntry(Path.GetFileName(file));

                    // Setup the entry data as required.

                    // Crc and size are handled by the library for seakable streams
                    // so no need to do them here.

                    // Could also use the last write time or similar for the file.
                    entry.DateTime = DateTime.Now;
                    s.PutNextEntry(entry);

                    using (FileStream fs = File.OpenRead(file))
                    {

                        // Using a fixed size buffer here makes no noticeable difference for output
                        // but keeps a lid on memory usage.
                        int sourceBytes;
                        do
                        {
                            sourceBytes = fs.Read(buffer, 0, buffer.Length);
                            s.Write(buffer, 0, sourceBytes);
                        } while (sourceBytes > 0);
                    }
                }

                // Finish/Close arent needed strictly as the using statement does this automatically

                // Finish is important to ensure trailing information for a Zip file is appended.  Without this
                // the created file would be invalid.
                s.Finish();

                // Close is important to wrap things up and unlock the file.
                s.Close();
            }
        }


        public static void openDir(string path)
        {
            Process.Start(@path);
        }

#if UNITY_STANDALONE_WIN
        // Get target of Windows .lnk file
        // Works wrong for links to files. Doesn't return last char
        public static string GetShortcutTarget(string file)
        {
            try
            {
                if (System.IO.Path.GetExtension(file).ToLower() != ".lnk")
                {
                    throw new Exception("Supplied file must be a .LNK file");
                }

                FileStream fileStream = File.Open(file, FileMode.Open, FileAccess.Read);
                using (System.IO.BinaryReader fileReader = new BinaryReader(fileStream))
                {
                    fileStream.Seek(0x14, SeekOrigin.Begin);     // Seek to flags
                    uint flags = fileReader.ReadUInt32();        // Read flags
                    if ((flags & 1) == 1)
                    {                      // Bit 1 set means we have to
                                           // skip the shell item ID list
                        fileStream.Seek(0x4c, SeekOrigin.Begin); // Seek to the end of the header
                        uint offset = fileReader.ReadUInt16();   // Read the length of the Shell item ID list
                        fileStream.Seek(offset, SeekOrigin.Current); // Seek past it (to the file locator info)
                    }

                    long fileInfoStartsAt = fileStream.Position; // Store the offset where the file info
                                                                 // structure begins
                    uint totalStructLength = fileReader.ReadUInt32(); // read the length of the whole struct
                    fileStream.Seek(0xc, SeekOrigin.Current); // seek to offset to base pathname
                    uint fileOffset = fileReader.ReadUInt32(); // read offset to base pathname
                                                               // the offset is from the beginning of the file info struct (fileInfoStartsAt)
                    fileStream.Seek((fileInfoStartsAt + fileOffset), SeekOrigin.Begin); // Seek to beginning of
                                                                                        // base pathname (target)
                    long pathLength = (totalStructLength + fileInfoStartsAt) - fileStream.Position - 2; // read
                                                                                                        // the base pathname. I don't need the 2 terminating nulls.
                    char[] linkTarget = fileReader.ReadChars((int)pathLength); // should be unicode safe
                    var link = new string(linkTarget);

                    int begin = link.IndexOf("\0\0");
                    if (begin > -1)
                    {
                        int end = link.IndexOf("\\\\", begin + 2) + 2;
                        end = link.IndexOf('\0', end) + 1;

                        string firstPart = link.Substring(0, begin);
                        string secondPart = link.Substring(end);

                        return firstPart + secondPart;
                    }
                    else
                    {
                        return link;
                    }
                }
            }
            catch
            {
                return "";
            }
        }
#endif

    }
}
