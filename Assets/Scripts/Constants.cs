﻿
namespace Assets.Scripts
{
    public class Constants
    {
        public const string EXT_PNG                 = ".png";
        public const string EXT_XML                 = ".xml";
        public const string EXT_LNK                 = ".lnk";

        public const string MSG_SAVED               = "Clothing saved";
        public const string MSD_NOT_SAVED           = "ERROR saving cloth settings";
        public const string MSD_IMAGE_NOT_SAVED     = "ERROR copying cloth image";
        public const string MSG_INCORRENT_FORMAT    = "Incorrect file format";
        public const string MSG_EMPTY_FIELD         = "Fields can't be empty";
        public const string MSG_NOTHING_TO_SAVE     = "Nothing to save";
        public const string MSG_NOTHING_TO_EDIT     = "Nothing to edit";
        public const string MSG_WRONG_LINK          = "Error opening link. Try goind straight to folder";
        public const string MSG_NOT_DIRECTORY       = "Selected link dosn't point to directory";
        public const string MSG_CAT_NAME_EMPTY      = "Category name can't be empty";
        public const string MSG_CAT_NAME_EXISTS     = "Category name already exists";
        public const string MSG_CAT_MIN_3           = "Must remain at least 3 categories";
        public const string MSG_CAT_ADDED           = "Category added";
        public const string MSG_NAME_EMPTY          = "Name can't be empty";
        public const string MSG_NEW_ITEM_ADDED      = "A new clothing is added to the collection";

        public const string IMAGE_FOLDER_NAME       = "Clothes";
        public const string DEFAULT_SETTINGS_NAME   = "ClothSettings.xml";

        public const float DEFAULT_OPACITY          = 0.7f;

        public const string ENG_FOLDER_NAME = "";
        public const string ACK_REMOVE = "Remove this clothing? You can't undo this action!";
        public const string ACK_REMOVE_TITLE = "Conformation";
        public const string ACK_REMOVE_LANG = "Remove current language? You can't undo this action!";
        public const string ACK_REMOVE_CAT = "Remove current category? You can't undo this action!";
    }
}
