﻿using UnityEngine;
using System.Collections.Generic;
using System;
using System.Linq;
//using Assets.Scripts;

[Serializable]
public class LanguageDescription
{
    public string languageId;
    public string languageName;
}

[Serializable]
public class Languages
{
    public List<LanguageDescription> descriptions = new List<LanguageDescription>();
    public Languages Clone()
    {
        return this.MemberwiseClone() as Languages;
    }
}

[Serializable]
public class ClothingStringData
{
    public string languageId;
    public string title = "jacket";
    public string description = "Price";
    public ClothingStringData CreateForNewLanguage(string newLangId)
    {
        return new ClothingStringData()
        {
            languageId = newLangId,
            title = title,
            description = description
        };
    }
}

[Serializable]
public class ClothingDescription
{
    public string clothingId;
    public string imageFileName;
    public ClothingDigitalData digitalData = new ClothingDigitalData();
    public List<ClothingStringData> stringData = new List<ClothingStringData>();
    public ClothingDescription()
    {

    }
    public ClothingDescription(string imageFile)
    {
        imageFileName = imageFile;
        clothingId = "{" + System.Guid.NewGuid().ToString().ToLower() + "}";
    }
    public ClothingStringData GetDataByLanguageId(string langId)
    {
        return stringData.First(p => p.languageId == langId);
    }
    public ClothingStringData CurrentStringData
    {
        get
        {
            return GetDataByLanguageId(LanguageParameterManager.instance.GetCurrentLanguageId());
        }
    }
    public ClothingDescription Clone()
    {
        return this.MemberwiseClone() as ClothingDescription;
    }
}

[Serializable]
public class ClothingDigitalData
{
    public string categoryId;
    public float scale = 1f;
    public float offsetX = 0f;
    public float offsetY = 0f;
    public bool topbased = false;
    // Sizes: XS S M L XL XXL
    public List<int> sizes = new List<int> ();
}

[Serializable]
public class ClothingData
{
    public List<ClothingDescription> data = new List<ClothingDescription>();
    [System.Xml.Serialization.XmlIgnore]
    private string currentClothingId;
    public void SetCurrentClothing(string clothId)
    {
        if (data.First(p => p.clothingId == clothId) != null)
        {
            currentClothingId = clothId;
        }
        else
        {
            Debug.Log("Clothing with id: " + clothId + " is absent!");
        }
    }
    public ClothingDescription GetDataByClothingId(string clothId)
    {
        return data.First(p => p.clothingId == clothId);
    }
    public ClothingDescription GetDataByImageFileName(string imageFileName)
    {
        return data.First(p => p.imageFileName == imageFileName);
    }
    public ClothingDescription CurrentClothingData
    {
        get
        {
            return GetDataByClothingId(currentClothingId);
        }
    }
    public ClothingData Clone()
    {
        return this.MemberwiseClone() as ClothingData;
    }
}

[Serializable]
public class CategoryStringData
{
    public string categoryId;
    public string title;
}

[Serializable]
public class CategoriesList
{
    public string languageId;
    public List<CategoryStringData> titles;
    public CategoriesList Clone()
    {
        return this.MemberwiseClone() as CategoriesList;
    }

}

[Serializable]
public class CategoriesData
{
    public List<CategoriesList> data = new List<CategoriesList>();
    public string GetCategoryById(string catId, string langId)
    {
        var cat = data.First(p => p.languageId == langId);
        if (cat != null)
        {
            return cat.titles.First(p => p.categoryId == catId).title;
        }
        else
        {
            return "";
        }
    }
    public string GetCategoryIdByName(string catTitle, string langId)
    {
        var cat = data.First(p => p.languageId == langId);
        if (cat != null)
        {
            return cat.titles.First(p => p.title == catTitle).categoryId;
        }
        else
        {
            return "";
        }
    }
    public CategoriesList CurrentCategories
    {
        get
        {
            return data.First(p => p.languageId == LanguageParameterManager.instance.GetCurrentLanguageId());
        }
    }

    public CategoriesData Clone()
    {
        return this.MemberwiseClone() as CategoriesData;
    }

}