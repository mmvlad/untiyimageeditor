﻿#if UNITY_STANDALONE_WIN

using System;
using System.Windows.Forms;
using System.IO;
using UnityEngine;

namespace Assets.Scripts
{
    public class WindowsFileSelector : IFileSelector
    {
        private string _filePath = "";
        private Action _onClose;
        private string _initialFolder = "";

        public WindowsFileSelector()
        {

        }
            
        public string getChoosenFilePath()
        {
            return _filePath;
        }

        public void showSelectFileDialog()
        {
            _filePath       = "";
            _initialFolder   = "";

            openDialog();
        }

        private void openDialog()
        {
            OpenFileDialog fd = new OpenFileDialog();
            fd.Filter = "Supported Files (.png and .lnk)|*.png; *.lnk;";

            if (_initialFolder.Length != 0)
            {
                fd.InitialDirectory = _initialFolder;
            }

            if (fd.ShowDialog() == DialogResult.OK)
            {
                _filePath = fd.FileName;

                // if link selected - check if folder
                if (Path.GetExtension(_filePath) == Constants.EXT_LNK)
                {
                    string target = Utils.GetShortcutTarget(_filePath);
                    
                    // check if link was parsed correctly
                    if (target.Length == 0)
                    {
                        showMessage(Constants.MSG_WRONG_LINK);
                        return;
                    }

                    //// if this is a link to png - open it
                    //if (Path.GetExtension(target) == Constants.EXT_PNG)
                    //{
                    //    _filePath = target;
                    //    return;
                    //}

                    if (!Directory.Exists(target))
                    {
                        showMessage(Constants.MSG_NOT_DIRECTORY);
                        return;
                    }

                    // if link to dir - open new dialog in that dir
                    _initialFolder = target;
                    openDialog();

                    return;
                }

                // if file selected  - fire event
                if (_onClose != null)
                {
                    _onClose();
                }
            }
        }

        public void setOnCloseEvent(Action onClose)
        {
            _onClose = onClose;
        }

        public void showMessage(string msg)
        {
            MessageBox.Show(msg);

        }

        public DialogResult acknowledge(string msg, string title)
        {
            return MessageBox.Show(msg, title, MessageBoxButtons.OKCancel, MessageBoxIcon.Warning);
        }

        private string openXmlDialog<T>(string filter) where T : FileDialog, new()
        {
            // create dialog - SaveFileDialog or OpenFileDialog
            T fd = new T();

            // .lnk files are supported for links to folders
            //fd.Filter = "Supported Files (.xml and .lnk)|*.xml; *.lnk;";
            fd.Filter = filter;

            if (_initialFolder.Length != 0)
            {
                fd.InitialDirectory = _initialFolder;
            }

            if (fd.ShowDialog() == DialogResult.OK)
            {
                string filePath = fd.FileName;

                // if link selected - check if folder
                if (Path.GetExtension(filePath) == Constants.EXT_LNK)
                {
                    string target = Utils.GetShortcutTarget(filePath);

                    // check if link was parsed correctly
                    if (target.Length == 0)
                    {
                        showMessage(Constants.MSG_WRONG_LINK);
                        return null;
                    }

                    //Selecting link to xml is not supported as used parser works incorrectly
                    //if (Path.GetExtension(target) == Constants.EXT_XML)
                    //{
                    //    return target;
                    //}

                    // if this link target is not xml or directory - fail
                    if (!Directory.Exists(target))
                    {
                        showMessage(Constants.MSG_NOT_DIRECTORY);
                        return null;
                    }

                    //looks like this link target is directory. open new dialog there
                    _initialFolder = target;

                    return openXmlDialog<T>(filter);
                }

                return filePath;
            }

            return null;
        }

        public string showOpenXmlDialog()
        {
            _initialFolder = "";

            return openXmlDialog<OpenFileDialog>("Supported Files (.xml and .lnk)|*.xml; *.lnk;");
        }

        public string showSaveDialog()
        {
            _initialFolder = "";

            return openXmlDialog<SaveFileDialog>("Supported Files (.xml and .lnk)|*.xml; *.lnk;");
        }


        public string showExportDialog()
        {
            _initialFolder = "";

            return openXmlDialog<SaveFileDialog>("Supported Files (.zip or .lnk)|*.zip; *.lnk;");
        }

        public string showSelectDirDialog()
        {
            FolderBrowserDialog openDir = new FolderBrowserDialog();

            DialogResult result = openDir.ShowDialog();
            if (result == DialogResult.OK)
            {
                string folderPath = openDir.SelectedPath;


                return folderPath;
            }

            return null;
        }
    }
}

#endif