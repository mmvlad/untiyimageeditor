﻿using UnityEngine;
using System.Collections.Generic;
using System.IO;
using System.Linq;

public class CommonParameterManager : ParameterManagerBase<CommonData>
{
    public static CommonParameterManager instance;

    void Awake()
    {
        instance = this;
    }

    void Start()
    {
        LoadIfNotLoaded();
    }

    public override void LoadIfNotLoaded()
    {
        base.LoadIfNotLoaded();
        if(IsLoaded)
            LanguageParameterManager.instance.SetCurrentLanguageById(parameters.activeLanguageId);
    }
}
