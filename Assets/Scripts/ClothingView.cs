﻿using UnityEngine;
using Assets.Scripts;
using System.IO;

public class ClothingView
{
    public GameObject model;
    public Texture2D texture;
    public string name;
    //private Clothing parameters;
    private ClothingDescription parameters;
    public string Title
    {
        get
        {
            return parameters.CurrentStringData.title;
        }
    }
    public string Description
    {
        get
        {
            return parameters.CurrentStringData.description;
        }
    }
    //public ClothingView(ref Clothing clothing, GameObject prefab)
    public ClothingView(ref ClothingDescription clothing, GameObject prefab)
    {
        parameters = clothing;
        name = parameters.imageFileName;
        model = GameObject.Instantiate(prefab) as GameObject;

        //model.GetComponent<ItemController>().view = this;
    }
    public bool LoadTexture()
    {
        var filePath = Path.Combine(PathLoader.GetImageFolder(), parameters.imageFileName);
        if (File.Exists(filePath))
        {
            var bytes = File.ReadAllBytes(filePath);
            texture = new Texture2D(1, 1);
            texture.LoadImage(bytes);
            //model.GetComponent<UITexture>().mainTexture = texture;
            model.SetActive(false);
            return true;
        }
        return false;
    }
}