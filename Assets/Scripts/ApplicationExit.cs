﻿using UnityEngine;
using System.Collections;
using System;
using System.Runtime.InteropServices;

public class ApplicationExit : MonoBehaviour {
    //Import window changing function
    [DllImport("USER32.DLL")]
    public static extern int SetWindowLong(IntPtr hWnd, int nIndex, int dwNewLong);

    //Import find window function
    [DllImport("user32.dll", EntryPoint = "FindWindow", SetLastError = true)]
    static extern IntPtr FindWindowByCaption(IntPtr ZeroOnly, string lpWindowName);

    //Import force window draw function
    [DllImport("user32.dll")]
    static extern bool DrawMenuBar(IntPtr hWnd);

    [DllImport("user32.dll", EntryPoint = "SetWindowPos")]
    public static extern IntPtr SetWindowPos(IntPtr hWnd, int hWndInsertAfter, int x, int Y, int cx, int cy, int wFlags);

    [DllImport("user32.dll", EntryPoint = "SetWindowPos")]
    public static extern int ShowWindow(IntPtr hWnd, int wFlags);


    private string WINDOW_NAME;            //name of the window
    private const int GWL_STYLE = -16;              //hex constant for style changing
    private const int WS_BORDER = 0x00800000;       //window with border
    private const int WS_CAPTION = 0x00C00000;      //window with a title bar
    private const int WS_SYSMENU = 0x00080000;      //window with no borders etc.
    private const int WS_MINIMIZEBOX = 0x00020000;  //window with minimizebox
    private const int SWP_SHOWWINDOW = 0x0040;      //displays the window  

    //public WindowState currentwinstate;


//    public ApplicationExit()
//    {
//    }

    /// <summary>
    /// Removes all the borders but keep it in a window that cant be resized.
    /// </summary>
    /// <param name="_width">This should be the screen's resolution width (Unity should provide a propper method for this)</param>
    /// <param name="_height">This should be the screen's resolution width (Unity should provide a propper method for this)</param>
    ///

//    private int _width = Screen.currentResolution.width;
//    private int _height = Screen.currentResolution.height;

    public void WindowedMaximized(int _width, int _height)
    {
        IntPtr window = FindWindowByCaption(IntPtr.Zero, WINDOW_NAME);
        SetWindowLong(window, GWL_STYLE, WS_SYSMENU);
        SetWindowPos(window, 0, 0, 0, _width, _height, SWP_SHOWWINDOW);
        DrawMenuBar(window);

    }
    public bool fullScreen = true;
    void Start()
    {
        WINDOW_NAME = "Clothing Editor";
        UnityEngine.Random.seed = (int)System.DateTime.Now.Ticks;
		if (fullScreen) {
			Screen.SetResolution (1080, 1920, true);
		} else {
			var res = Screen.resolutions;
			Screen.SetResolution(res[res.Length - 1].width, res[res.Length - 1].height - 100, false);
            Invoke("Maximize", 2f);
        }
    }

    void Maximize()
    {
        IntPtr window = FindWindowByCaption(IntPtr.Zero, WINDOW_NAME);
        Debug.Log(window == IntPtr.Zero ? "Wnd is not found" : "Wnd is found");
        //ShowWindow(window, 2);
    }

    // Update is called once per frame
    void Update () {
		if (Input.GetKey(KeyCode.Escape)) {
			Application.Quit();
		}	
	}

}
