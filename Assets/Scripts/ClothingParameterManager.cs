﻿using UnityEngine;
using System.Collections.Generic;
using System;
using System.Linq;

public class ClothingParameterManager : ParameterManagerBase<ClothingData>
{
    public static ClothingParameterManager instance;

    void Awake()
    {
        instance = this;
    }
    // Use this for initialization
    void Start()
    {
        LoadIfNotLoaded();
    }
}
