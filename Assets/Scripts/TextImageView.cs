﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class TextImageView : MonoBehaviour
{

    public Image image;
    public Text text;

    public void setData(Sprite sprite, string imageName)
    {
        image.sprite    = sprite;
        text.text       = imageName;
    }
 
}
