﻿using System;
using System.IO;
using System.Collections.Generic;

namespace Assets.Scripts.Legacy
{
    [Serializable]
    public class Clothing
    {
        public string imageFileName;
        public string title;
        public string description;
        public float scale = 1.0f;
        public float offsetX = 0;
        public float offsetY = 0;
        public string category = "";
        public bool topbased = false;
        public List<int> sizes = new List<int>() { 0, 1, 2, 3, 4, 5 };
        public Clothing()
        {

        }

        public Clothing(string imageName)
        {
            imageFileName = imageName;
            title = Path.GetFileNameWithoutExtension(imageFileName);
            description = title;
        }


        public override string ToString()
        {
            return "File name: " + imageFileName + "\n"
                + "Title: " + title + "\n"
                + "Desc: " + description
                + "Cat: " + description;
        }

        public Clothing ShallowCopy()
        {
            return this.MemberwiseClone() as Clothing;
        }

    }
}
