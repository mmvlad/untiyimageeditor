﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;

[RequireComponent(typeof(Button))]
public class ComboBox : MonoBehaviour
{

    [SerializeField]
    private Image scrollPanel;

    private Image currentPanel;

    void Awake()
    {
        Image i = Instantiate(scrollPanel);
        i.transform.SetParent(transform);
        i.transform.localPosition = Vector3.zero;

        float width = GetComponent<RectTransform>().sizeDelta.x;

        Vector2 panelSize = i.rectTransform.sizeDelta;
        panelSize.x = width;
        i.rectTransform.sizeDelta = panelSize;

        i.gameObject.SetActive(false);

        EventTrigger trigger        = gameObject.AddComponent<EventTrigger>();
        EventTrigger.Entry entry    = new EventTrigger.Entry();
        entry.eventID               = EventTriggerType.PointerExit;
        entry.callback.AddListener((eventData) => {
            i.gameObject.SetActive(false);
        });

        trigger.triggers.Add(entry);

        Button btn = GetComponent<Button>();
        btn.onClick.AddListener(() => {
            i.gameObject.SetActive(!i.gameObject.activeSelf);
        });

        currentPanel = i;

        init();
    }

    private void init()
    {
        TextImageComboBox tt = GetComponent<TextImageComboBox>();
        tt.setPanel(currentPanel.gameObject);
    }

  
}
