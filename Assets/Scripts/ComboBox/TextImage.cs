﻿
using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public class TextImage
{
    public Sprite Sprite;
    public string Name;

    public TextImage(string name, Sprite sprite)
    {
        Sprite  = sprite;
        Name    = name;
    }

    public TextImage()
    {
        Sprite = null;
        Name = "---";
    }
}

