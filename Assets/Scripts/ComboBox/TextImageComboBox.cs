﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine.UI;
using UnityEngine;

public class TextImageComboBox : ComboBoxController<TextImage>
{

    public override void ConfigureView(Button btn, TextImage option)
    {
        Image[] img = btn.GetComponentsInChildren<Image>();
        Debug.Log("Imges found: " + img.Length);
        //img.sprite = option.Sprite;
        Text name = GetComponentInChildren<Text>();
        name.text = option.Name;
    }

}

