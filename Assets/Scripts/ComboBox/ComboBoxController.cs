﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using UnityEngine;
using UnityEngine.UI;

public abstract class ComboBoxController<T> : MonoBehaviour where T : new()
{
    [SerializeField]
    private GameObject _panel;
    [SerializeField]
    private Button _btnPref;

    private List<string> _options = new List<string>();

    private ContentSizeFitter _fitter;

    private List<T> _opts = new List<T>();


    public abstract void ConfigureView(Button btn, T option);

    public void setPanel(GameObject o)
    {
        _panel = o;
    }

    //public abstract void updateCurrent()

    public void addOpt(T option)
    {
        _panel.SetActive(true);

        Button btn = Instantiate(_btnPref);
        ConfigureView(btn, option);

        btn.onClick.AddListener(() => {
            ConfigureView(GetComponent<Button>(), option);
            _panel.SetActive(false);
        });

        if (_fitter == null)
        {
            _fitter = _panel.GetComponentInChildren<ContentSizeFitter>();
        }
        
        btn.transform.SetParent(_fitter.transform);

        _opts.Add(option);

        updateFirst();

        _panel.SetActive(false);
    }


    public void addOption(string name)
    {
        int index = _options.FindIndex(x => x == name);

        if (index >= 0)
        {
            Debug.LogWarning("Item already in list. Wont be added");
            return;
        }

        _panel.SetActive(true);

        ContentSizeFitter fitter = _panel.GetComponentInChildren<ContentSizeFitter>();

        Button btn = Instantiate(_btnPref);

        Text txt = btn.GetComponentInChildren<Text>();
        txt.text = name;

        btn.onClick.AddListener(() => {
           // _choiceText.text = btn.GetComponentInChildren<Text>().text;
            _panel.SetActive(false);
        });


        btn.transform.SetParent(fitter.transform);

        _options.Add(name);

        updateFirst();

        _panel.SetActive(false);
    }

    private void updateFirst()
    {
        ConfigureView(GetComponent<Button>(), _opts[0]);
        //_choiceText.text = _options[0];
    }


    public void clearAll()
    {
        _opts.Clear();

        _panel.SetActive(true);

        foreach (Transform t in _fitter.transform)
        {
            Destroy(t.gameObject);
        }

        _panel.SetActive(false);

        ConfigureView(GetComponent<Button>(), new T());
    }

    public T getSelectedOption()
    {
        return _opts[getSelectedIndex()];
    }

  
    public int getSelectedIndex()
    {
        //int res = _options.FindIndex(x => x == getSelectedOptionName());

        return 0;
    }

    public void removeOptionByName(string name)
    {
        int index = _options.FindIndex(x => x == name);
        if (index == -1)
        {
            return;
        }

        _options.Remove(name);

        _panel.SetActive(true);

        ContentSizeFitter fitter = _panel.GetComponentInChildren<ContentSizeFitter>();

        foreach (Transform t in fitter.transform)
        {
            Text btnText = t.GetComponentInChildren<Text>();
            if (btnText.text == name)
            {
                Destroy(t.gameObject);
                break;
            }
        }
        if (_options.Count > 0)
        {
            updateFirst();
        }
        else
        {
            ConfigureView(GetComponent<Button>(), new T());
        }


        _panel.SetActive(false);
    }

    public void removeOptionByIndex(int index)
    {
        if (index < 0 || index >= _options.Count)
        {
            Debug.LogError("Index out of range");
            return;
        }

        string name = _options[index];
        removeOptionByName(name);
    }

    public void removeSelected()
    {
        //removeOptionByName(getSelectedOptionName());
    }

    public List<string> ListOfOptions
    {
        get
        {
            return new List<string>(_options);
        }

        set
        {
            List<string> newList = value;
            clearAll();

            foreach (string option in newList)
            {
                addOption(option);
            }
        }
    }

}
