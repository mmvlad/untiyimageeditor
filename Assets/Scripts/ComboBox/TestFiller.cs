﻿using UnityEngine;
using System.Collections;

public class TestFiller : MonoBehaviour
{

    public TextImage[] options;


    void Start()
    {
        TextImageComboBox controller = GetComponent<TextImageComboBox>();

        foreach (var option in options)
        {
            controller.addOpt(option);
        }

        Destroy(this);
    }
}
