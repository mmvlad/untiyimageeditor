﻿using UnityEngine;
using System.Collections;
using System;
using System.Linq;

public class LanguageParameterManager : ParameterManagerBase<Languages>
{
    public static LanguageParameterManager instance;
    public Action<string> OnChangeLanguage;
    // Default language is English
    [HideInInspector]
    public string defaultLanguageID = "{08a98034-a6e4-4116-85df-924822bf5766}";
    private string currentLanguageId;
    void Awake()
    {
        instance = this;
    }

    void Start()
    {
        LoadIfNotLoaded();
    }

    public override void LoadIfNotLoaded()
    {
        if (!IsLoaded)
        {
            currentLanguageId = defaultLanguageID;
        }
        base.LoadIfNotLoaded();
    }

    public bool SetCurrentLanguageById(string langId)
    {
        if (Parameters.descriptions.First(p => p.languageId == langId) != null)
        {
            currentLanguageId = langId;
            if (OnChangeLanguage != null)
            {
                OnChangeLanguage(currentLanguageId);
            }
            return true;
        }
        else {
            return false;
        }
    }

    public bool SetCurrentLanguageByName(string langName)
    {
        var newLang = Parameters.descriptions.First(p => p.languageName == langName);
        if (newLang != null)
        {
            currentLanguageId = newLang.languageId;
            if (OnChangeLanguage != null)
            {
                OnChangeLanguage(currentLanguageId);
            }
            return true;
        }
        else {
            return false;
        }
    }

    public string GetCurrentLanguageId()
    {
        LoadIfNotLoaded();
        return currentLanguageId;
    }

    public string GetCurrentLanguageName()
    {
        return Parameters.descriptions.First(p => p.languageId == GetCurrentLanguageId()).languageName;
    }
}
