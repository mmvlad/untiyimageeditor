﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;

public class PseudoText : Text {

    public delegate string GetPseudo(string alias);
    public GetPseudo OnGetPseudo;
    public override string text
    {
        get
        {
            if (OnGetPseudo != null)
            {
                return OnGetPseudo(base.text);
            }
            else
                return base.text;   
        }

        set
        {
            base.text = value;
        }
    }

}
