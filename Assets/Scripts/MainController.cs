﻿using UnityEngine;
using System.Collections.Generic;
using Assets.Scripts;
using System.IO;
using UnityEngine.UI;
using System;
using System.Xml;
using System.Linq;
using StringDic = System.Collections.Generic.Dictionary<string, string>;

public class MainController : MonoBehaviour
{
    [SerializeField]
    private Image _image;
    [SerializeField]
    private Text _title;
    [SerializeField]
    private Text _desc;
    [SerializeField]
    private GameObject _editSettingWidnow;
    [SerializeField]
    private GameObject _editMetaWidnow;
    [SerializeField]
    private InputField _titleField;
    [SerializeField]
    private InputField _descField;
    [SerializeField]
    private Text _category;
    [SerializeField]
    private GameObject _startWindow;
    [SerializeField]
    private MyComboBoxController _metaCategoryBox;
    [SerializeField]
    private GameObject _editCatWidnow;
    [SerializeField]
    private GameObject _editLangWindow;
    [SerializeField]
    private InputField _newCatNameField;
    [SerializeField]
    private InputField _newLangNameField;
    [SerializeField]
    private MyComboBoxController _catEditBox;
    [SerializeField]
    private MyComboBoxController _langEditBox;
    [SerializeField]
    private MyComboBoxController _langSelectBox;
    [SerializeField]
    private MyComboBoxController _langSelectBoxInCat;
    [SerializeField]
    private MyComboBoxController _langSelectBoxInTips;
    [SerializeField]
    private GameObject _listView;
    [SerializeField]
    private GameObject _showBtn;
    [SerializeField]
    private GameObject _hideBtn;
    [SerializeField]
    private GameObject _descWindow;
    [SerializeField]
    private GameObject _showDescBtn;
    [SerializeField]
    private GameObject _hideDescBtn;
    [SerializeField]
    private InputField _tip1Field;
    [SerializeField]
    private InputField _tip2Field;
    [SerializeField]
    private List<Toggle> sizes;
    [SerializeField]
    private Toggle _firstCategory;

    public Transform leftShoulder;
    public Transform rightShoulder;
    public Transform centerShoulder;
    public Transform hipCenter;
    public RectTransform backgroundImage;

    public static MainController instance;

    private float _scaleCorrection = 1f;
    private float _xCorrection = 0f;
    private float _yCorrection = 0f;
    private ClothingData cacheClothes;
    private Languages cacheLanguages;
    private CategoriesData cacheCategories;
    private CommonData cacheCommon;

    private ClothController _clothController;
    private IFileSelector _fileSelector;
    private ClothingDescription _currentClothing;
    private ClothingDescription _changeClothing;
    private ClothingData _clothesList;
    private string _settingsPath = "";
    private ClothingData _clothes;

    private string settingsPath;
    private EditorParameters parameters;
    private float _deltaX;
    private float _deltaY;
    private float _corrScale = 1;
    private float _offsetX;
    private float _offsetY;
    private string _currentLang = Constants.ENG_FOLDER_NAME;
    private ClothList _list;
    private bool _topbased = false;

    public bool isTopbased
    {
        get { return _topbased; }
    }

    void Awake()
    {
        instance = this;
        _list = FindObjectOfType<ClothList>();
        ClothingParameterManager.instance.OnLoad += OnLoadParameters;
        CommonParameterManager.instance.OnLoad += OnLoadParameters;
        CategoriesParameterManager.instance.OnLoad += OnLoadParameters;
        LanguageParameterManager.instance.OnLoad += OnLoadParameters;
    }

    void Start()
    {
        settingsPath = Path.Combine(PathLoader.rootPath, "ed_parameters.xml");
        if (File.Exists(settingsPath))
        {
            try
            {
                parameters = UnityXMLSerializer.DeserializeFromXMLFile<EditorParameters>(settingsPath);
            }
            catch (Exception)
            {
                parameters = new EditorParameters();
                UnityXMLSerializer.SerializeToXMLFile<EditorParameters>(settingsPath, parameters);
            }
        }
        else
        {
            parameters = new EditorParameters();
            UnityXMLSerializer.SerializeToXMLFile<EditorParameters>(settingsPath, parameters);
        }

        backgroundImage.localScale = new Vector3(parameters.windowScale, parameters.windowScale, parameters.windowScale);
        backgroundImage.localPosition += new Vector3(0, parameters.windowVerticalOffset, 0);

        LoadLanguages();

        _scaleCorrection = parameters._scaleCorrection;
        _xCorrection = parameters._xCorrection;
        _yCorrection = parameters._yCorrection;
#if UNITY_STANDALONE_WIN
        _fileSelector = new WindowsFileSelector();
#endif

        if (_fileSelector == null)
        {
            Debug.LogError("File selector not created");
            return;
        }

        if (_image == null)
        {
            Debug.LogError("Target image is missing");
            return;
        }

        _clothController = FindObjectOfType<ClothController>();
        _clothController.MotionStep = parameters.motionStep;
        _clothController.ScaleStep = parameters.scaleStep;
        if (_clothController == null)
        {
            Debug.LogError("ClothController is missing");
            return;
        }

        _fileSelector.setOnCloseEvent(onCloseAction);

        _startWindow.SetActive(false);

        hideListView();
        hideMetaData();
        _deltaX = Mathf.Abs(rightShoulder.localPosition.x - leftShoulder.localPosition.x);
        _deltaY = Mathf.Abs(hipCenter.localPosition.y - centerShoulder.localPosition.y);
        _langEditBox.OnSelectFinish += OnSelectLanguageOnEditLang;
        _langSelectBox.OnSelectFinish += OnSelectLanguageOnEdit;
        _langSelectBoxInCat.OnSelectFinish += OnSelectLanguageOnEditCat;
        _langSelectBoxInTips.OnSelectFinish += OnSelectLanguageOnEditTips;
        _metaCategoryBox.OnSelectFinish += OnSelectCategory;
        _catEditBox.OnSelectFinish += OnSelectNextCategory;
        CorrectJoints();
    }

    public void OnSelectNextCategory(string text)
    {
        var idx = cacheCategories.data[0].titles.FindIndex(x => x.categoryId == text);
        _firstCategory.isOn = idx == 1;
    }

    // On select category in editing matadata of clothing
    public void OnSelectCategory(string text)
    {
        var selectedCat = CategoriesParameterManager.instance.parameters.GetCategoryById(text,
            LanguageParameterManager.instance.GetCurrentLanguageId());
        _changeClothing.digitalData.categoryId = text;
    }

    public void onDescFieldChanged()
    {
        _changeClothing.CurrentStringData.description = _descField.text;
    }

    public void onTitleFieldChanged()
    {
        _changeClothing.CurrentStringData.title = _titleField.text;
    }

    void OnSelectLanguageOnEdit(string text)
    {
        SetNewCurrentLanguage(text);
        _metaCategoryBox.DictionaryOfOptions = CategoriesParameterManager.instance.GetCategoriesForCurrentLanguage();
        if (_changeClothing != null)
        {
            resetFields();
        }
    }

    void OnSelectLanguageOnEditCat(string text)
    {
        SetNewCurrentLanguage(text);

        _clothesList = _clothes;
        _catEditBox.DictionaryOfOptions = CategoriesParameterManager.instance.GetCategoriesForCurrentLanguage();
        ;
        _metaCategoryBox.DictionaryOfOptions = CategoriesParameterManager.instance.GetCategoriesForCurrentLanguage();
    }

    void OnSelectLanguageOnEditTips(string text)
    {
        SetNewCurrentLanguage(text);
        updateTips(text);
    }

    void OnSelectLanguageOnEditLang(string text)
    {
        SetNewCurrentLanguage(text);
        _langSelectBox.setText(LanguageParameterManager.instance.GetCurrentLanguageName());
        _langSelectBoxInCat.setText(LanguageParameterManager.instance.GetCurrentLanguageName());
        _langSelectBoxInTips.setText(LanguageParameterManager.instance.GetCurrentLanguageName());
    }

    private void SetNewCurrentLanguage(string text)
    {
        _currentLang = text;
        LanguageParameterManager.instance.SetCurrentLanguageById(text);
        CreateClothesList();
    }

    private void LoadLanguages()
    {
        //languages.Clear();
        //var dirs = Directory.GetDirectories(PathLoader.languagePath);
        //foreach (var item in dirs)
        //{
        //    languages.Add(Path.GetFileNameWithoutExtension(item));
        //}
        var currentLang = LanguageParameterManager.instance.GetCurrentLanguageId();
        var langs = LanguageParameterManager.instance.parameters.descriptions.ToDictionary(p => p.languageId,
            p => p.languageName);
        _langEditBox.DictionaryOfOptions = langs;
        _langSelectBoxInCat.DictionaryOfOptions = langs;
        _langSelectBox.DictionaryOfOptions = langs;
        _langSelectBoxInTips.DictionaryOfOptions = langs;
    }

    void Update()
    {
        Vector3 localPos = _image.transform.localPosition;
        //_corrX = centerShoulder.transform.localPosition.x - _image.transform.localPosition.x;
        //_corrY = centerShoulder.transform.localPosition.y - _image.transform.localPosition.y;
        //var rect = _image.GetComponent<RectTransform>();
        //_corrScale = (rect.sizeDelta.x * rect.localScale.x / _deltaX) * _scaleCorrection;
        //_offsetX = _corrX / _corrScale;
        //_offsetY = _corrY / _corrScale - _deltaY / 2 + _yCorrection;
        //var X = middlePointView.x + _offsetX * shoulderScale;//centerShoulder.transform.localPosition.x;
        //var Y = middlePointView.y + _offsetY * shoulderScale;
        //var S = _corrScale * shoulderScale;
        _corrScale = _image.transform.localScale.x / shoulderScale;
        _offsetX = (localPos.x - middlePointView.x) / shoulderScale;
        _offsetY = (localPos.y - middlePointView.y) / shoulderScale;
    }

    private void OnLoadParameters()
    {
        if (CategoriesParameterManager.instance.IsLoaded && ClothingParameterManager.instance.IsLoaded &&
            LanguageParameterManager.instance.IsLoaded && CommonParameterManager.instance.IsLoaded)
        {
            CreateClothesList();
        }
    }

    void CreateClothesList()
    {
        _clothes = ClothingParameterManager.instance.parameters;
        if (_currentLang == "")
        {
            _currentLang = LanguageParameterManager.instance.defaultLanguageID;
        }
        _list.Clear();
        if (_clothes != null)
        {
            string cat = "";
            for (int i = 0; i < _clothes.data.Count; ++i)
            {
                ClothingDescription item = _clothes.data[i];
                if (cat != item.digitalData.categoryId)
                {
                    cat = item.digitalData.categoryId;
                    _list.addSeparator(CategoriesParameterManager.instance.parameters.GetCategoryById(cat, _currentLang));
                }
                _list.addButton(item);
            }
        }
        _clothesList = _clothes;
    }

    Vector3 middlePoint = Vector3.zero;
    private float deltaShoulder = 0f;
    private Vector3 middlePointView = Vector3.zero;
    private float deltaShoulderView;
    private float shoulderScale;

    void CorrectJoints()
    {
        var joints = CommonParameterManager.instance.parameters.digitalData;
        middlePoint = Vector3.Lerp(joints.leftShoulderJoint, joints.rightShoulderJoint, 0.5f);
        deltaShoulder = Mathf.Abs(joints.rightShoulderJoint.x - joints.leftShoulderJoint.x);
        var deltaNeck = joints.neckJoint - middlePoint;
        var deltaSpine = joints.baseSpineJoint - middlePoint;
        deltaShoulderView = Mathf.Abs((rightShoulder.localPosition - leftShoulder.localPosition).x);
        middlePointView = Vector3.Lerp(rightShoulder.localPosition, leftShoulder.localPosition, 0.5f);

        shoulderScale = deltaShoulderView / deltaShoulder;

        centerShoulder.localPosition = new Vector3(centerShoulder.localPosition.x,
            shoulderScale * deltaNeck.y + middlePointView.y, 0);
        hipCenter.localPosition = new Vector3(hipCenter.localPosition.x, shoulderScale * deltaSpine.y + middlePointView.y,
            0);
    }

    void OnApplicationQuit()
    {
        UnityXMLSerializer.SerializeToXMLFile<EditorParameters>(settingsPath, parameters);
    }

    /// <summary>
    /// Gets the cloth list.
    /// </summary>
    /// <returns></returns>
    public List<ClothingDescription> getClothList()
    {
        return _clothes.data;
    }
    /// <summary>
    /// Sets the clothing.
    /// </summary>
    /// <param name="selectedClothing">The selectedClothing.</param>
    public void setClothing(ClothingDescription selectedClothing)
    {
        if (_currentClothing != null)
        {
            updateClothingVals();
        }

        _currentClothing = selectedClothing;
        cacheClothes = _clothes.Clone();

        _corrScale = _currentClothing.digitalData.scale;
        _offsetX = _currentClothing.digitalData.offsetX;
        _offsetY = _currentClothing.digitalData.offsetY;
        selectJoints(_currentClothing.digitalData.topbased);
        string imagePath = PathLoader.GetImageFolder() + Path.DirectorySeparatorChar + selectedClothing.imageFileName;
        generateImageFromFile(imagePath);
        var rect = _image.GetComponent<RectTransform>();
        var X = middlePointView.x + _offsetX * shoulderScale;
        var Y = middlePointView.y + _offsetY * shoulderScale;
        var S = _corrScale * shoulderScale;
        _clothController.applyProps(X, Y, S);

        setTransparent();
        updateMetaDataUI();
    }

    /// <summary>
    /// Resets the session.
    /// </summary>
    public void resetSession()
    {
        _currentClothing = null;
        _clothesList = null;
        _settingsPath = "";

        _startWindow.SetActive(true);
    }

    /// <summary>
    /// Gets the name of the settings file.
    /// </summary>
    /// <returns></returns>
    private string getSettingsFileName()
    {
        return Path.GetFileNameWithoutExtension(_settingsPath);
    }

    /// <summary>
    /// Changes the name of the settings file.
    /// </summary>
    /// <param name="newName">The new name.</param>
    private void changeSettingsFileName(string newName)
    {
        string dir = Path.GetDirectoryName(_settingsPath);
        _settingsPath = dir + Path.DirectorySeparatorChar + newName + Constants.EXT_XML;
    }

    // BUTTON ACTION    
    /// <summary>
    /// Opens the BTN action.
    /// </summary>
    public void openBtnAction()
    {
        _fileSelector.showSelectFileDialog();
    }

    /// <summary>
    /// Updates the tips.
    /// </summary>
    /// <param name="text">The text.</param>
    void updateTips(string text)
    {
        //if (_tip1Field.text != "")
        //{
        //    cacheParams[currentLang].Hints[0] = _tip1Field.text;
        //}
        //if (_tip2Field.text != "")
        //{
        //    cacheParams[currentLang].Hints[1] = _tip2Field.text;
        //}
        //currentLang = text;
        //_tip1Field.text = cacheParams[text].Hints[0];
        //_tip2Field.text = cacheParams[text].Hints[1];
        // TODO: Update common data from labels
    }


    // BUTTON ACTION    
    /// <summary>
    /// Selects the joints.
    /// </summary>
    /// <param name="center">if set to <selectedClothing>true</selectedClothing> [center].</param>
    public void selectJoints(bool center)
    {
        var color = centerShoulder.GetComponent<Image>().color;
        centerShoulder.GetComponent<Image>().color = new Color(color.r, color.g, color.b, center ? 1f : 0.5f);
        hipCenter.GetComponent<Image>().color = new Color(color.r, color.g, color.b, center ? 1f : 0.5f);
        rightShoulder.GetComponent<Image>().color = new Color(color.r, color.g, color.b, !center ? 1f : 0.5f);
        leftShoulder.GetComponent<Image>().color = new Color(color.r, color.g, color.b, !center ? 1f : 0.5f);
        _topbased = center;
    }

    // BUTTON ACTION
    public void editSettingBtnAction()
    {
        _editSettingWidnow.SetActive(true);
        //cacheParams = new Dictionary<string, CommonParameters>(_dictParams);
        //TODO: create Common data cache

        updateTips(_currentLang);
    }

    // BUTTON ACTION    
    /// <summary>
    /// Saves the updated settings.
    /// </summary>
    public void saveUpdatedSettings()
    {
        //TODO: Update Common data from labels

        saveSettingsFile();
        _tip1Field.text = "";
        _tip2Field.text = "";
    }

    // BUTTON ACTION
    public void cancelUpdatedSettings()
    {
        _editSettingWidnow.SetActive(false);
    }

    // BUTTON ACTION    
    /// <summary>
    /// Edits the BTN action.
    /// </summary>
    public void editBtnAction()
    {
        if (_currentClothing == null)
        {
            showMessage(Constants.MSG_NOTHING_TO_EDIT);
            return;
        }
        _changeClothing = _currentClothing.Clone();
        var szs = _changeClothing.digitalData.sizes;
        sizes.ForEach(p => p.isOn = false);
        for (int i = 0; i < szs.Count(); i++)
        {
            if (szs[i] < sizes.Count())
            {
                sizes[szs[i]].isOn = true;
            }
        }

        _editMetaWidnow.SetActive(true);
        OnSelectLanguageOnEdit(_currentLang);
        _langSelectBox.setText(LanguageParameterManager.instance.GetCurrentLanguageName());
    }

    // BUTTON ACTION    
    /// <summary>
    /// Saves the meta BTN action.
    /// </summary>
    public void saveMetaBtnAction()
    {
        string newTitle = _titleField.text;
        string newDesc = _descField.text;
        string newCat = _metaCategoryBox.getSelectedOptionName();

        if (newTitle.Length == 0 || newDesc.Length == 0 || newCat.Length == 0)
        {
            showMessage(Constants.MSG_EMPTY_FIELD);
            return;
        }
        ChangeSizes();
        var idx =
            ClothingParameterManager.instance.parameters.data.FindIndex(p => p.clothingId == _changeClothing.clothingId);
        ClothingParameterManager.instance.parameters.data[idx] = _changeClothing;
        updateMetaDataUI();

        _editMetaWidnow.SetActive(false);
        CreateClothesList();
    }

    // BUTTON ACTION    
    /// <summary>
    /// Cancels the meta BTN a ction.
    /// </summary>
    public void cancelMetaBtnACtion()
    {
        _editMetaWidnow.SetActive(false);
    }

    private void resetFields()
    {
        _titleField.text = _changeClothing.CurrentStringData.title;
        _descField.text = _changeClothing.CurrentStringData.description;
        _metaCategoryBox.setText(
            CategoriesParameterManager.instance.parameters.GetCategoryById(_changeClothing.digitalData.categoryId,
                _currentLang));
    }

    // event when file choose dialog is closed and file is choosen    
    /// <summary>
    /// Ons the close action.
    /// </summary>
    private void onCloseAction()
    {
        // starting point after opening the image
        string path = _fileSelector.getChoosenFilePath();
        string imageName = Path.GetFileName(path);

        if (!Utils.IsCorrectFormat(path))
        {
            showMessage(Constants.MSG_INCORRENT_FORMAT);
            return;
        }
        var exist = _clothesList.data.Find(p => p.imageFileName == imageName);
        // Add new picture to existig file settings
        if (exist != null)
        {
            _fileSelector.showMessage("Current clothing \"" + exist.CurrentStringData.title + "\" is already added!");
            return;
        }

        // change this if you need to treat loaded image as new one
        loadClothingSettings(path);

        // Copy image 
        Utils.copyTo(path, PathLoader.GetImageFolder() + Path.DirectorySeparatorChar + imageName);

        updateMetaDataUI();

        generateImageFromFile(path);

        save();
        setTransparent();
        CreateClothesList();
        showMessage(Constants.MSG_NEW_ITEM_ADDED);
    }
    /// <summary>
    /// Saves this instance.
    /// </summary>
    public void save()
    {
        if (_currentClothing != null)
        {
            updateClothingVals();
        }

        saveSettingsFile();
    }

    /// <summary>
    /// Exits this instance.
    /// </summary>
    public void exit()
    {
        Application.Quit();
    }

    // export to zip
    public void export()
    {
        string patchFilePath = _fileSelector.showExportDialog();

        if (patchFilePath == null)
        {
            return;
        }

        // temp dir to copy files there before export
        if (Directory.Exists(PathLoader.getTempDir()))
        {
            Directory.Delete(PathLoader.getTempDir(), true);
        }

        Directory.CreateDirectory(PathLoader.getTempDir());

        // copy files from settings to tmep dir
        foreach (ClothingDescription item in _clothesList.data)
        {
            Utils.copyTo(PathLoader.getImagePath(item.imageFileName), PathLoader.getTempImagePath(item.imageFileName));
        }

        Utils.CompressDirToZip(PathLoader.getTempDir(), patchFilePath);

        Directory.Delete(PathLoader.getTempDir(), true);

        Utils.openDir(Path.GetDirectoryName(patchFilePath));
    }


    private void generateImageFromFile(string path)
    {
        Sprite s = Utils.SpriteFromFile(path);
        _image.rectTransform.sizeDelta = new Vector2(s.rect.width, s.rect.height); // set image to original dimensions
        _image.sprite = s;
    }

    /// <summary>
    /// Loads the clothing settings.
    /// </summary>
    /// <param name="path">The path.</param>
    private void loadClothingSettings(string path)
    {
        string imageName = Path.GetFileName(path);

        bool exists = false;

        if (_currentClothing != null)
        {
            exists = _clothesList.data.Exists(x => x.imageFileName == imageName);
        }
        // TODO: add case when the cloth will be first
        else
        {
            //_currentClothing = new ClothingDescription(imageName);
            //_currentClothing.digitalData.categoryId = CategoriesParameterManager.instance.parameters.data[0].titles[0].categoryId;
        }

        if (exists)
        {
            // clone object so settings aren't applier until we save it
            _currentClothing = _clothesList.data.Find(x => x.imageFileName == imageName).Clone();
            Debug.Log("Using existing image data");
        }
        else
        {
            _currentClothing = ClothingParameterManager.instance.parameters.data[0].Clone();
            _currentClothing.clothingId = ClothingParameterManager.CreateId();
            _currentClothing.imageFileName = imageName;
            _clothesList.data.Add(_currentClothing);
            cacheClothes = _clothesList.Clone();
            _clothController.reset();
            Debug.Log("Creating new cloth desc");
        }
    }

    /// <summary>
    /// Updates the meta data UI.
    /// </summary>
    private void updateMetaDataUI()
    {
        _title.text = _currentClothing.CurrentStringData.title;
        _desc.text = _currentClothing.CurrentStringData.description;
        _category.text =
            CategoriesParameterManager.instance.parameters.GetCategoryById(_currentClothing.digitalData.categoryId,
                _currentLang);
    }

    // apply image transform to Clothing object    
    /// <summary>
    /// Updates the clothing vals.
    /// </summary>
    private void updateClothingVals()
    {
        _currentClothing.digitalData.scale = _corrScale;
        _currentClothing.digitalData.offsetX = _offsetX;
        _currentClothing.digitalData.offsetY = _offsetY;
        _currentClothing.digitalData.topbased = _topbased;
    }


    // BUTTON ACTION    
    /// <summary>
    /// Saves the BTN action.
    /// </summary>
    public void saveBtnAction()
    {
        if (_currentClothing == null)
        {
            showMessage(Constants.MSG_NOTHING_TO_SAVE);
            return;
        }

        // write scale, offset, etc to Clothing object
        updateClothingVals();

        // add clothing to list
        commitClothing();

        // serialize list
        saveSettingsFile();
    }

    // BUTTON ACTION
    public void removeBtnAction()
    {
        if (_currentClothing != null)
        {
            if (_fileSelector.acknowledge(Constants.ACK_REMOVE, Constants.ACK_REMOVE_TITLE) ==
                System.Windows.Forms.DialogResult.OK)
            {
                Debug.Log("Clothing " + _currentClothing.CurrentStringData.title + " was removed!");
                if (CheckLastClothingInCategory())
                {
                    ClothingParameterManager.instance.Parameters.data.RemoveAll(
                p => p.clothingId == _currentClothing.clothingId);
                    _image.sprite = null;
                    Color c = Color.white;
                    c.a = 0;
                    _image.color = c;
                    Utils.delete(Path.Combine(PathLoader.GetImageFolder(), _currentClothing.imageFileName));
                    _currentClothing = null;
                    _title.text = "-";
                    _desc.text = "-";
                    _category.text = "-";
                    save();
                    CreateClothesList();

                }
            }
            else
            {
                Debug.Log("Remove action canceled.");
            }
        }
    }

    private bool CheckLastClothingInCategory()
    {
        // todo: Check is deleting item last?
        var currentDirectory = _currentClothing.digitalData.categoryId;
        //if (cacheCategories.CurrentCategories[])
        //{
           
        //}
        return true;
    }

    /// <summary>
    /// Commits the clothing.
    /// </summary>
    private void commitClothing()
    {
        //TODO: Commit changes

        //bool exists = _clothesList.clothes.Exists(x => x.imageFileName == _currentClothing.imageFileName);

        //if (exists)
        //{
        //    // remove previous data
        //    _clothesList.clothes.RemoveAll(x => x.imageFileName == _currentClothing.imageFileName);
        //    Debug.Log("Saving existing clothing");
        //}
        //else
        //{
        //    Debug.Log("Saving new clothing");
        //}

        //// add new
        //_clothesList.clothes.Add(_currentClothing);
    }

    private bool copyClothImage()
    {
        try
        {
            string settingsFileDir = Path.GetDirectoryName(_settingsPath);
            string destFile = settingsFileDir + Path.DirectorySeparatorChar + _currentClothing.imageFileName;

            if (!File.Exists(destFile))
            {
                Debug.Log("File doesn't exist");
                Utils.copyTo(_fileSelector.getChoosenFilePath(), destFile);
            }

            return true;
        }
        catch (System.Exception e)
        {
            Debug.Log(e.ToString());
            return false;
        }
    }
    /// <summary>
    /// Saves the settings file.
    /// </summary>
    private void saveSettingsFile()
    {
        LanguageParameterManager.instance.Save();
        CategoriesParameterManager.instance.Save();
        ClothingParameterManager.instance.Save();
        CommonParameterManager.instance.Save();
    }
    /// <summary>
    /// Sets the transparent.
    /// </summary>
    private void setTransparent()
    {
        Color c = Color.white;
        c.a = Constants.DEFAULT_OPACITY;
        _image.color = c;
    }
    /// <summary>
    /// Shows the message.
    /// </summary>
    /// <param name="msg">The MSG.</param>
    private void showMessage(string msg)
    {
        _fileSelector.showMessage(msg);
    }

    /// <summary>
    /// Gets the settings folder.
    /// </summary>
    /// <returns></returns>
    private string getSettingsFolder()
    {
        return Path.GetDirectoryName(_settingsPath);
    }
    /// <summary>
    /// Shows the ListView.
    /// </summary>
    public void showListView()
    {
        _listView.SetActive(true);
        _showBtn.SetActive(false);
        _hideBtn.SetActive(true);
    }
    /// <summary>
    /// Hides the ListView.
    /// </summary>
    public void hideListView()
    {
        _listView.SetActive(false);
        _showBtn.SetActive(true);
        _hideBtn.SetActive(false);
    }
    /// <summary>
    /// Shows the meta data.
    /// </summary>
    public void showMetaData()
    {
        _descWindow.SetActive(true);
        _showDescBtn.SetActive(false);
        _hideDescBtn.SetActive(true);
    }
    /// <summary>
    /// Hides the meta data.
    /// </summary>
    public void hideMetaData()
    {
        _descWindow.SetActive(false);
        _showDescBtn.SetActive(true);
        _hideDescBtn.SetActive(false);
    }
    /// <summary>
    /// Saves the cache.
    /// </summary>
    private void SaveCache()
    {
        LanguageParameterManager.instance.parameters = cacheLanguages;
        _clothesList = cacheClothes;
        ClothingParameterManager.instance.parameters = _clothesList;
        CategoriesParameterManager.instance.parameters = cacheCategories;
        CommonParameterManager.instance.parameters = cacheCommon;
    }
    /// <summary>
    /// Creates the cache.
    /// </summary>
    private void CreateCache()
    {
        cacheLanguages = LanguageParameterManager.instance.parameters.Clone();
        cacheClothes = _clothesList.Clone();
        cacheCategories = CategoriesParameterManager.instance.parameters.Clone();
        cacheCommon = CommonParameterManager.instance.parameters.Clone();
    }

    #region Category management    
    /// <summary>
    /// Sets the first category.
    /// </summary>
    public void SetFirstCategory()
    {
        if (_firstCategory.isOn)
        {
            var selected = _catEditBox.getSelectedId();
            // TODO: move selected ID to first position
            var cats = cacheCategories.data;
            foreach (var item in cats)
            {
                var idx = item.titles.FindIndex(x => x.categoryId == selected);
                var moved = item.titles[idx];
                item.titles.RemoveAt(idx);
                item.titles.Insert(1, moved);
            }
        }
    }

    // BUTTON ACTION    
    /// <summary>
    /// Adds the new cat.
    /// </summary>
    public void addNewCat()
    {
        string catName = _newCatNameField.text;
        if (catName.Length == 0)
        {
            showMessage(Constants.MSG_CAT_NAME_EMPTY);
            return;
        }
        if (cacheCategories.CurrentCategories.titles.Exists(p => p.title == catName))
        {
            showMessage(Constants.MSG_CAT_NAME_EXISTS);
            return;
        }
        var newCatId = CategoriesParameterManager.CreateId();
        cacheCategories.data.ForEach(
            p => p.titles.Add(new CategoryStringData() { categoryId = newCatId, title = catName }));
        _catEditBox.addOption(newCatId, catName);
        _newCatNameField.text = "";
        showMessage(Constants.MSG_CAT_ADDED);
    }

    // BUTTON ACTION    
    /// <summary>
    /// Removes the current cat.
    /// </summary>
    public void removeCurrentCat()
    {
        if (cacheCategories.data[0].titles.Count > 3)
        {
            if (_fileSelector.acknowledge(Constants.ACK_REMOVE_CAT, Constants.ACK_REMOVE_TITLE) ==
                System.Windows.Forms.DialogResult.OK)
            {
                string catName = _catEditBox.getSelectedOptionName();
                var cat = cacheCategories.CurrentCategories.titles.First(p => p.title == catName);
                var catId = cat.categoryId;
                cacheCategories.data.ForEach(d => d.titles.RemoveAll(p => p.categoryId == catId));
                _catEditBox.DictionaryOfOptions = CategoriesParameterManager.instance.GetCategoriesForCurrentLanguage();
                cacheClothes.data.RemoveAll(p => p.digitalData.categoryId == catId);
            }
        }
        else
        {
            showMessage(Constants.MSG_CAT_NAME_EXISTS);
        }
    }

    // BUTTON ACTION    
    /// <summary>
    /// Renames the current cat.
    /// </summary>
    public void renameCurrentCat()
    {
        //TODO: Rename current category
        string catName = _newCatNameField.text;
        if (cacheCategories.CurrentCategories.titles.Exists(p => p.title == catName))
        {
            showMessage(Constants.MSG_CAT_NAME_EXISTS);
        }
        else
        {
            cacheCategories.CurrentCategories.titles.First(p => p.title == _catEditBox.getSelectedOptionName()).title =
                catName;
            _catEditBox.DictionaryOfOptions = CategoriesParameterManager.instance.GetCategoriesForCurrentLanguage();
            _catEditBox.setText(catName);
        }
    }


    // BUTTON ACTION    
    /// <summary>
    /// Saves the updated cats.
    /// </summary>
    public void saveUpdatedCats()
    {
        SaveCache();
        CreateClothesList();
        _editCatWidnow.SetActive(false);
    }


    // BUTTON ACTION    
    /// <summary>
    /// Edits the cats action.
    /// </summary>
    public void editCatsAction()
    {
        if (_clothesList == null)
        {
            showMessage(Constants.MSG_NOTHING_TO_EDIT);
            return;
        }
        CreateCache();
        // first show window so all elements are active
        _editCatWidnow.SetActive(true);
        _catEditBox.DictionaryOfOptions = CategoriesParameterManager.instance.GetCategoriesForCurrentLanguage();
        OnSelectNextCategory(_catEditBox.getSelectedId());
        _newCatNameField.text = "";
    }

    #endregion

    #region Language management

    // BUTTON ACTION    
    /// <summary>
    /// Edits the language action.
    /// </summary>
    public void editLangAction()
    {
        // first show window so all elements are active
        _editLangWindow.SetActive(true);
        _newLangNameField.text = "";
        CreateCache();
    }


    // BUTTON ACTION    
    /// <summary>
    /// Adds the new language.
    /// </summary>
    public void addNewLang()
    {
        string langName = _newLangNameField.text;
        var langList = cacheLanguages.descriptions.Select(p => p.languageName).ToList();
        langList = langList.ConvertAll(p => p.ToLower());
        if (langList.Contains(langName.ToLower()))
        {
            _fileSelector.showMessage("Language " + langName + " has already existed! Try another name.");
            Debug.Log("Language " + langName + " exists! Try another name.");
        }
        else
        {
            var newLang = new LanguageDescription()
            {
                languageId = LanguageParameterManager.CreateId(),
                languageName = langName
            };
            cacheLanguages.descriptions.Add(newLang);

            var newCommonStringData = cacheCommon.CurrentStringData.Clone();
            newCommonStringData.languageId = newLang.languageId;
            cacheCommon.stringData.Add(newCommonStringData);

            var newCategories = cacheCategories.CurrentCategories.Clone();
            newCategories.languageId = newLang.languageId;
            cacheCategories.data.Add(newCategories);

            cacheClothes.data.ForEach(
                p => p.stringData.Add(p.CurrentStringData.CreateForNewLanguage(newLang.languageId)));

            var langs = cacheLanguages.descriptions.ToDictionary(p => p.languageId, p => p.languageName);
            _langEditBox.DictionaryOfOptions = langs;
        }
    }

    // BUTTON ACTION
    public void removeCurrentLang()
    {
        if (_fileSelector.acknowledge(Constants.ACK_REMOVE_LANG, Constants.ACK_REMOVE_TITLE) ==
            System.Windows.Forms.DialogResult.OK)
        {
            string langName = _langEditBox.getSelectedOptionName();
            Debug.Log("Language \"" + langName + "\" is removed");
            var langId = cacheLanguages.descriptions.First(p => p.languageName == langName).languageId;

            cacheLanguages.descriptions.RemoveAll(p => p.languageId == langId);

            cacheCommon.stringData.RemoveAll(p => p.languageId == langId);

            cacheCategories.data.RemoveAll(p => p.languageId == langId);

            cacheClothes.data.ForEach(p => p.stringData.RemoveAll(d => d.languageId == langId));
            var langs = cacheLanguages.descriptions.ToDictionary(p => p.languageId, p => p.languageName);
            _langEditBox.DictionaryOfOptions = langs;
        }
    }

    // BUTTON ACTION    
    /// <summary>
    /// Saves the updated languages.
    /// </summary>
    public void saveUpdatedLanguages()
    {
        SaveCache();
        _editLangWindow.SetActive(false);
    }

    #endregion

    #region Sizes

    void ChangeSizes()
    {
        if (_changeClothing != null)
        {
            _changeClothing.digitalData.sizes.Clear();
            for (int i = 0; i < sizes.Count(); i++)
            {
                if (sizes[i].isOn)
                {
                    _changeClothing.digitalData.sizes.Add(i);
                }
            }
        }
    }

    #endregion
}

[Serializable]
public class EditorParameters
{
    public float _scaleCorrection = 0.65f;
    public float _xCorrection = -4.104f;
    public float _yCorrection = -52.05f;
    public float kscale = 1f;
    public float bscale = 0f;
    public float kx = 1f;
    public float bx = 0f;
    public float ky = 1f;
    public float by = 0f;
    public float windowScale = 3f;
    public float windowVerticalOffset = 100f;
    public float scaleStep = 0.05f;
    public float motionStep = 0.1f;
}