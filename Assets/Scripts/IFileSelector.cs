﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Assets.Scripts
{
    /// <summary>
    /// Interface for platform dependant file selector
    /// </summary>
    interface IFileSelector
    {
        void showSelectFileDialog();
        string getChoosenFilePath();
        void setOnCloseEvent(Action onClose);
        void showMessage(string msg);
        DialogResult acknowledge(string msg, string title);

        string showSaveDialog();
        string showOpenXmlDialog();

        string showSelectDirDialog();
        string showExportDialog();
    }
}
