﻿using UnityEngine;
using System.IO;
using System;
using Assets.Scripts;

public class PathLoader : MonoBehaviour
{
    public static string commonDataPath;
    public static string userDocumentsPath;
    public static string userDesktopPath;
    public static string adjustMenuSettingsPath;
    public static string userApplicationDataPath;
    public static string rootFolderName;
    public static string rootPath;
    public static string clothesSettingsPath;
    public const string productName = "MagicMirror";

    public const string clothesSettingsFileName = "ClothesSettings.xml";
    public const string commonSettingsFileName = "CommonSettings.xml";
    /// <summary>
    /// Awakes this instance.
    /// </summary>
    private void Awake()
    {
        userDocumentsPath = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
        userDesktopPath = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
        userApplicationDataPath = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);

        rootFolderName = Application.productName;
        rootPath = Path.Combine(userApplicationDataPath,
            Application.companyName + Path.DirectorySeparatorChar + productName);
        commonDataPath = rootPath;
        print("Clothes folder is " + rootPath);
        if (!Directory.Exists(PathLoader.rootPath))
        {
            Directory.CreateDirectory(PathLoader.rootPath);
        }

        if (!Directory.Exists(GetImageFolder()))
        {
            Directory.CreateDirectory(GetImageFolder());
        }

        clothesSettingsPath = Path.Combine(rootPath, clothesSettingsFileName);
    }
    /// <summary>
    /// Gets the image folder.
    /// </summary>
    /// <returns>Image folder path.</returns>
    public static string GetImageFolder()
    {
        return Path.Combine(rootPath, Constants.IMAGE_FOLDER_NAME);
    }
    /// <summary>
    /// Gets the temporary dir.
    /// </summary>
    /// <returns>Temporary folder path.</returns>
    public static string getTempDir()
    {
        return Path.Combine(PathLoader.rootPath, "Temp");
    }

    public static string getImagePath(string imageName)
    {
        return Path.Combine(PathLoader.GetImageFolder(), imageName);
    }

    public static string getTempImagePath(string imageName)
    {
        return Path.Combine(getTempDir(), imageName);
    }

    public static string getPatchFileName()
    {
        return Path.Combine(PathLoader.rootPath, "Patch.zip");
    }
}