﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.UI;
using Assets.Scripts;
using System.IO;
using System;
using System.Linq;

public class ClothList : MonoBehaviour
{
    [SerializeField]
    private Button _btnPref;
    [SerializeField]
    private GameObject _sepPref;

    private ContentSizeFitter _fitter;

    Dictionary<string, Button> btns = new Dictionary<string, Button>();
    Dictionary<string, GameObject> seps = new Dictionary<string, GameObject>();
    void Awake()
    {
        _fitter = GetComponentInChildren<ContentSizeFitter>();
    }

    public void addSeparator(string title)
    {
        var sep = Instantiate(_sepPref);
        sep.GetComponentInChildren<Text>().text = title;
        sep.transform.SetParent(_fitter.transform);
        seps[title] = sep;
    }

    public void addButton(ClothingDescription cloth)
    {
        string imageFile = PathLoader.GetImageFolder() + Path.DirectorySeparatorChar + cloth.imageFileName;
        if (!File.Exists(imageFile))
        {
            Debug.LogError("File : " + imageFile + " doest NOT exist");
            return;
        }

        Button btn = Instantiate(_btnPref);
        btns[cloth.imageFileName] = btn;

        TextImageView view = btn.GetComponent<TextImageView>();
        Sprite s = Utils.SpriteFromFile(imageFile);

        view.setData(s, cloth.CurrentStringData.title);

        btn.GetComponent<Button>().onClick.AddListener(() =>
        {
            MainController.instance.setClothing(cloth);
        });

        
        btn.transform.SetParent(_fitter.transform);
    }

    public void removeBtn(ClothingDescription cloth)
    {
        var obj = btns[cloth.imageFileName].gameObject;
        obj.transform.SetParent(null);
        btns.Remove(cloth.imageFileName);
        Destroy(obj);
    }

    public void Clear()
    {
        while (btns.Count > 0)
        {
            var key = btns.Keys.First();
            var obj = btns[key].gameObject;
            obj.transform.SetParent(null);
            btns.Remove(key);
            Destroy(obj);
        }
        while (seps.Count > 0)
        {
            var key = seps.Keys.First();
            var obj = seps[key].gameObject;
            obj.transform.SetParent(null);
            seps.Remove(key);
            Destroy(obj);
        }
    }
}
